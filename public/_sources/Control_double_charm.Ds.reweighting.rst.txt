Ds.reweighting package
======================

.. mdinclude:: ../../python/Control_double_charm/Ds/reweighting/README.md

.. automodule:: Ds.reweighting
   :members:
   :undoc-members:
   :show-inheritance:

Scripts
-------

Ds.reweighting.compare\_cor\_vs\_uncor module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.compare_cor_vs_uncor
   :members:
   :undoc-members:
   :show-inheritance:

Ds.reweighting.compare\_MC\_vs\_data module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.compare_MC_vs_data
   :members:
   :undoc-members:
   :show-inheritance:

Ds.reweighting.reweighting module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.reweighting
   :members:
   :undoc-members:
   :show-inheritance:

Ds.reweighting.apply\_weights\_full\_MC module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.apply_weights_full_MC
   :members:
   :undoc-members:
   :show-inheritance:

Ds.reweighting.apply\_weights\_signal module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.apply_weights_signal
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

Ds.reweighting.definition module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.definition
   :members:
   :undoc-members:
   :show-inheritance:

Ds.reweighting.build\_distribution module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.build_distribution
   :members:
   :undoc-members:
   :show-inheritance:

Ds.reweighting.functions\_reweighting module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.functions_reweighting
   :members:
   :undoc-members:
   :show-inheritance:

Ds.reweighting.parser\_def module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.parser_def
   :members:
   :undoc-members:
   :show-inheritance:

Ds.reweighting.reweighting\_columns module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: Ds.reweighting.reweighting_columns
   :members:
   :undoc-members:
   :show-inheritance:
