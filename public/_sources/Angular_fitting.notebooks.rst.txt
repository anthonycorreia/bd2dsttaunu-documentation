Angular\_fitting.notebooks package
=========================================

.. mdinclude:: ../../python/Angular_fitting/notebooks/README.md

.. automodule:: Angular_fitting.notebooks
   :members:
   :undoc-members:
   :show-inheritance:
